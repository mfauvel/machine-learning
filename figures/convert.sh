for file in *tex
do
    pdflatex -shell-escape ${file}
    pdf2svg ${file%%.tex}.pdf ${file%%.tex}.svg
done

rm *log *aux *pdf
